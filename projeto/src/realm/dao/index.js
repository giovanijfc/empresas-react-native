import getRealm from '../../services/realm'

class BaseDAO {

    async deleteAll() {
        await getRealm().then((realm) => realm.write(() => realm.deleteAll()));
    }

    async saveUser(user, headers) {
        const userToSave = {
            id: user.investor.id,
            email: user.investor.email,
            token: headers.get("Access-Token"),
            client: headers.get("client"),
            name: user.investor.investor_name,
            balance: user.investor.balance,
            city: user.investor.city,
            country: user.investor.country,
            expireToken: headers.get("expiry"),
        };

        await getRealm().then((realm) => {
            realm.write(() => {
                realm.create('User', userToSave);
            })
        });
    }
}

export default new BaseDAO();