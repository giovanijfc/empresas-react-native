export default class UserSchema {
    static schema = {
        name: 'User',
        primaryKey: 'id',
        properties: {
            id: {type: 'int', indexed: true, default: 0},
            email: 'string' ,
            token: 'string' ,
            client: 'string' ,
            name: 'string' ,
            balance: {type: 'int', default: 0},
            city: 'string',
            country: 'string',
            expireToken: 'string',
        }
    };
}