import Snackbar from 'react-native-snackbar';

class MessageUtil {

    showShort(title) {
        Snackbar.show({
            title,
            duration: Snackbar.LENGTH_SHORT,
        });
    }

    showLong(title) {
        Snackbar.show({
            title,
            duration: Snackbar.LENGTH_LONG,
        });
    }
}

export default new MessageUtil();