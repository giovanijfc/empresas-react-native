import styled from 'styled-components/native';

export const Container = styled.View`
flex: 1;
margin-left: 6%;
margin-right: 6%;
`;

export const ListEnterprises = styled.ScrollView`
margin-top: 7px;
`;

export const HeaderSearch = styled.View`
flex-direction: row;
align-items: center;
margin-top: 1%;
`;

export const NotFound = styled.Text`
margin: 0 auto;
font-size: 16px;
font-weight: bold;
margin-top: 85%;
`;



