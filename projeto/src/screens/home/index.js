import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import BaseAPI from '../../services/api'
import { Container, HeaderSearch, NotFound } from './styles';
import { Input } from '../../components/input';
import Card from '../../components/card/index';
import CardHolderContent from '../../components/card_holder_content/index';
import {
  StatusBar,
  StyleSheet,
  Alert,
  FlatList,
} from 'react-native';
import IconAntDesign from 'react-native-vector-icons/AntDesign';
import IconEntypo from 'react-native-vector-icons/Entypo';
import { loggedOut } from '../../store/actions/user';

const placeHolderQuantity = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15];

export default Home = ({ navigation }) => {

  const [enterprises, setEnterprises] = useState([]);
  const [textFind, setTextFind] = useState();
  const [typesToSave, setTypesToSave] = useState();
  const [inLoading, setInLoading] = useState();
  const [userLogged, setUserLogged] = useState(useSelector((state) => state.credentials.userLogged));
  const [auth, setAuth] = useState(
    useSelector((state) => {
      return {
        accessToken: state.credentials.userLogged.token,
        client: state.credentials.userLogged.client,
        uid: state.credentials.userLogged.email,
        expiry: state.credentials.userLogged.expiry
      }
    })
  );
  const dispatch = useDispatch();

  Home.navigationOptions = ({ navigation }) => {
    return {
      title: `Bem vindo, ${navigation.getParam("userName", "")}`,
      headerRight: () =>
        (
          <IconAntDesign name="logout" size={24}
            style={{ marginRight: 16 }}
            onPress={() => navigation.getParam("handleLogout")()} />
        )
    };
  };

  useEffect(() => {
    getEnterprises();
    navigation.setParams({ userName: userLogged.name, handleLogout });
  }, []);

  const getEnterprises = async () => {
    setInLoading(true);
    await BaseAPI.getAll(auth).then(
      (response) => setEnterprises(response.enterprises));
    setInLoading(false);
  }

  const getEnterprisesByFilter = async (text) => {
    setInLoading(true);
    await BaseAPI.getByTypeOrName(text, auth).then(
      (response) => setEnterprises(response.enterprises));
    setInLoading(false);
  }

  const handleChangeText = async (text) => {
    if (text.length === 0) {
      await getEnterprises();
    } else {
      await getEnterprisesByFilter(text)
    }
  }

  const handleLogout = () => {
    Alert.alert(
      'Sair',
      'Tem certeza que você deseja sair?',
      [
        {
          text: 'Sim', onPress: () => {
            dispatch(loggedOut())
            navigation.replace("Login");
          },
        },
        {
          text: 'Não', onPress: () => { }, style: 'cancel',
        },
      ],
      { cancelable: false },
    );
  }

  return (
    <Container>
        <StatusBar backgroundColor="black" barStyle="light-content" />

        <HeaderSearch>
          <IconEntypo name="magnifying-glass" size={30} />
          <Input style={styled.input} onChangeText={(text) => handleChangeText(text)}
            placeholder="Busque empresas pelo nome..." />
        </HeaderSearch>

        {
          inLoading
            ? placeHolderQuantity.map((item) => (<CardHolderContent key={item} />))
            :
            enterprises
              &&
              enterprises.length > 0
              ? <FlatList
                data={enterprises}
                renderItem={({ item }) => (<Card key={item.id} enterprise={item} navigation={navigation} />)}
              />
              : <NotFound>Nenhuma empresa encontrada!</NotFound>
        }
    </Container>
  )
}

const styled = StyleSheet.create({
  input: {
    marginTop: 10,
    color: '#424242',
    flex: 1,
  },
  icon: {
    height: 25,
    width: 25,
  }
});


