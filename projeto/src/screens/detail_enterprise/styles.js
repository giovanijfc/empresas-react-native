import styled from 'styled-components/native';

export const Container = styled.View`
flex: 1;
background: #FFF;
padding: 6%;
`;

export const ListEnterprises = styled.ScrollView`
margin-top: 7px;
`;

export const HeaderBack = styled.View`
flex-direction: row;
`;

export const Body = styled.View`
flex: 1;
flex-direction: column;
margin-top: 30%;
justify-content: center;
`;

export const Card = styled.View`
background-color: black;
width: 100%;
margin-top: 12px;
border-radius: 4px;
padding: 6%;
padding-bottom: 10%;
`;

export const Title = styled.Text`
font-size: 21px;
font-weight: bold;
color: black;
margin: 0 auto;
`;

export const Localization = styled.Text`
font-size: 14px;
font-weight: bold;
color: white;
`;

export const Description = styled.Text`
font-size: 14px;
color: white;
margin-top: 3%;
`;

export const Price = styled.Text`
font-size: 16;
color: green;
`;

export const Type = styled.Text`
margin-top: 3%
font-size: 16;
color: #CAE1FF;
`;




