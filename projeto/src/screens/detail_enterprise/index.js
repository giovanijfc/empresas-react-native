import React, { useState } from 'react';
import { StatusBar, SafeAreaView } from 'react-native';
import {
  Container,
  HeaderBack,
  Title,
  Body,
  Card,
  Localization,
  Description,
  Price,
  Type
} from './styles';
import IconFontisto from 'react-native-vector-icons/Fontisto';

export default function DetailEnterprise({ navigation }) {

  const [enterprise] = useState(navigation.getParam("enterprise"));

  return (
    <Container>
      <SafeAreaView />
        <StatusBar backgroundColor="white" barStyle="dark-content" />

        <HeaderBack>
          <IconFontisto onPress={() => navigation.pop()}
            color="black" size={34} name="arrow-left" />
        </HeaderBack>

        <Body>
          <Title>{enterprise.enterprise_name}</Title>

          <Card>

            <Localization>
              Localização: {enterprise.city}, {enterprise.country}.
          </Localization>

            <Description>
              Descrição: {enterprise.description}
            </Description>

            <Type>
              Tipo: {enterprise.enterprise_type.enterprise_type_name}
            </Type>

            <Price>
              Preço: ${enterprise.share_price}
            </Price>
          </Card>
        </Body>
    </Container>
  );

}

