import React, { useState } from 'react';
import { StatusBar, 
    StyleSheet, 
    Text, 
    TouchableOpacity 
} from 'react-native';
import { Container, Form } from './styles';
import { ButtonMedium } from '../../components/button';
import { Input } from '../../components/input';
import { TextMedium } from '../../components/text';
import MessageUtil from '../../util/message';
import BaseAPI from '../../services/api';
import { Keyboard } from 'react-native';
import { useDispatch } from 'react-redux';
import { loggedIn } from '../../store/actions/user';
import getRealm from '../../services/realm';
import BaseDAO from '../../realm/dao/index';

export default function login({ navigation }) {

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [secondInput, setSecondInput] = useState(null);
    const [loading, setLoading] = useState(false);
    const dispatch = useDispatch();

    async function handleLogin() {
        setLoading(true);
        if (validateFields()) {
            await Keyboard.dismiss();
            await BaseAPI.login(email, password).then((response) => response.json().then((user) => {
                if (user.success) {
                    BaseDAO.saveUser(user, response.headers);
                } else {
                    if (user.errors[0] === "Invalid login credentials. Please try again.") {
                        MessageUtil.showShort("Credenciais incorretas, tente novamente.");
                    } else {
                        MessageUtil.showShort("Aconteceu um erro ao logar, tente novamente.");
                    }
                }
            })).catch((error) => {
                console.log(error);
                MessageUtil.showShort("Aconteceu um erro ao logar, tente novamente.");
            });

            let userLogged = await getRealm().then((realm) => realm.objects("User"));
            console.log(userLogged);
            if (userLogged[0]) {
                dispatch(loggedIn(userLogged[0]));
                navigation.replace("Home");
            }

        }
        setLoading(false);
    }

    function validateFields() {
        if (email && password) {
            return true;
        } else {
            MessageUtil.showShort("Não deixe nenhum campo vazio!");
            return false;
        }
    }

    return (
        <Container>
            <StatusBar backgroundColor="black" barStyle="light-content" />
            <Form>
                <TextMedium style={styled.text}>LOGIN</TextMedium>

                <Input type={"email"} autoFocus={true} style={styled.input}
                    placeholder='exemplo@exemplo.com' onChangeText={text => setEmail(text)}
                    onSubmitEditing={() => { secondInput.focus(); }} blurOnSubmit={false} />
                <Input secureTextEntry={true} style={styled.input}
                    placeholder='********' onChangeText={text => setPassword(text)}
                    ref={(input) => { setSecondInput(input) }} />
                <TouchableOpacity disabled={loading} activeOpacity={0.7}
                style={{...styled.buttonLogin, opacity: loading ? 0.5 : 1}} onPress={() => handleLogin()}>
                    <Text style={styled.textButtonLogin}>{loading ? 'Carregando...' : 'Entrar'}</Text>
                </TouchableOpacity>
            </Form>
        </Container>);
}

const styled = StyleSheet.create({
    input: {
        marginBottom: 8,
    },
    text: {
        marginBottom: 16,
    },
    buttonLogin: {
        padding: 10,
        alignItems: "center",
        backgroundColor: '#000000'  , 
    },
    textButtonLogin : {
        fontWeight: '500',
        fontSize: 15,
        color: '#ffffff',
    }
});
