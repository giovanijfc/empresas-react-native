import React, { useEffect } from 'react';
import { StatusBar } from 'react-native';
import { Container, Logo } from './styled';
import getRealm from '../../services/realm';
import { useDispatch } from 'react-redux';
import { authIn } from '../../store/actions/user'

export default function Splash({ navigation }) {

  const dispatch = useDispatch();

  useEffect(() => {
    async function checkIfUserIsLogged() {
      let userLogged = await getRealm().then((realm) => realm.objects("User"));

      setTimeout(() => {
        dispatch(authIn(userLogged[0], navigation));
        if (userLogged[0]) {
          navigation.replace("Home");
        } else {
          navigation.replace("Login");
        }
      }, 1300);
    }

    checkIfUserIsLogged();
  }, []);

  return (
    <Container>
      <StatusBar backgroundColor="white" barStyle="light-content" />
      <Logo source={require('../../assets/images/logo_ioasys.png')} />
    </Container>
  );
}