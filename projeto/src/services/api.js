const BASE_URL = "https://empresas.ioasys.com.br/api/v1";

class BaseAPI {

  async getAll(auth) {
    return fetch(`${BASE_URL}/enterprises`, {
      method: "GET",
      headers: this.getObrigatoryHeader(auth),
    }).then((response) => response.json());
  }

  async getById(id) {
    return fetch(`${BASE_URL}/enterprises/${id}`);
  }

  async getByTypeOrName(name, auth) {
    return fetch(`${BASE_URL}/enterprises?name=${name}`, {
      method: "GET",
      headers: this.getObrigatoryHeader(auth),
    }).then((response) => response.json());
  }

  async login(email, password) {
    let credentials = { 'email': email, 'password': password }

    let data = {
      method: 'POST',
      body: JSON.stringify(credentials),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      }
    }

    return fetch(`${BASE_URL}/users/auth/sign_in`, data).then((response) => response);
  }

  getObrigatoryHeader(auth) {
    return {
      'Content-Type': 'application/json',
      'access-token': auth.accessToken,
      'client': auth.client,
      'uid': auth.uid,
    }
  }
}

export default new BaseAPI();