import Realm from 'realm';
import User from '../realm/schemas/user'

export default function getRealm() {
    return Realm.open({ schema: [User] });
}