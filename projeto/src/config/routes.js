import Splash from '../screens/splash';
import Login from '../screens/login';
import Home from '../screens/home';
import DetailEnterprise from '../screens/detail_enterprise';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

const MainNavigator = createStackNavigator(
  {
    Splash: {
      screen: Splash,
      navigationOptions: ({ navigation }) => ({
        header: null
      }),
    },
    Login: {
      screen: Login,
      navigationOptions: ({ navigation }) => ({
        header: null
      }),
    },
    Home,
    DetailEnterprise: {
      screen: DetailEnterprise,
      navigationOptions: ({ navigation }) => ({
        header: null
      }),
    },
  },
  {
    initialRouteName: 'Splash',
  },
);

const App = createAppContainer(MainNavigator);

export default App;
