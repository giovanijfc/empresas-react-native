import styled from 'styled-components/native';

export const Container = styled.View.attrs({
    elevation: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,
})
    `
background-color: #CDCDCD;
border-radius: 6px;
padding-top: 7%;
text-align: center;
padding-bottom: 10%
margin-bottom: 8%;
`;

export const Margin = styled.View`
padding: 0px 16px 0px 16px;
`;

export const Description = styled.Text`
height: 200;
margin-top: 10px;
font-size: 14;
background-color: #999999;
`;

export const Name = styled.Text`
margin-top: 5px
font-size: 22;
padding: 5%;
background-color: #999999;
`;

export const BottomAlign = styled.View`
width: 100%;
height: 35;
position: absolute; 
bottom: 0;
`;