import React from 'react';
import { Container, Name, Description, BottomAlign, Margin } from './styles';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

export default function CardHolderContent() {

    return (
        <Container>
            <Margin>
                <Name></Name>
                <Description></Description>
            </Margin>

            <BottomAlign>
                <TouchableOpacity
                    style={styled.buttonLogin} onPress={() => handleDetailEnterprise()}>
                    <Text style={styled.textButtonLogin}>DETALHES</Text>
                </TouchableOpacity>
            </BottomAlign>
        </Container>
    );
}

const styled = StyleSheet.create({
    buttonLogin: {
        padding: 10,
        alignItems: "center",
        backgroundColor: '#000000',
    },
    textButtonLogin: {
        fontWeight: '500',
        fontSize: 15,
        color: '#ffffff',
    }
});