import styled from 'styled-components/native';

export const Input = styled.TextInput.attrs({
    keyboardAppearance: "dark",
})
`
color: black;
border-color: black;
border-width: 1;
border-radius: 4;
font-size: 15px;
padding: 10px 10px 10px 10px;
`;