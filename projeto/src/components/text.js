import styled from 'styled-components/native';

export const TextMedium = styled.Text`
color: black;
font-size: 29px;
font-weight: bold;
text-align: center;
`;