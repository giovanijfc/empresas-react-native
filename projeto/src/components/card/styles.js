import styled from 'styled-components/native';

export const Container = styled.View.attrs({
    elevation: 5,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.8,
    shadowRadius: 1,  
})
`
background-color: #F5F5F5;
border-radius: 6px;
padding-top: 7%;
text-align: center;
padding-bottom: 10%
margin-bottom: 8%;
`;

export const Margin = styled.View`
padding: 0px 16px 0px 16px;
`;

export const Description = styled.Text`
margin-top: 10px
font-size: 14;
margin-bottom: 3%;
`;

export const Name = styled.Text`
margin-top: 10px
font-size: 22;
`;

export const BottomAlign = styled.View`
width: 100%;
height: 35;
position: absolute; 
bottom: 0;
`;