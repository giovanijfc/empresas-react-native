import { createStore, combineReducers } from 'redux';
import userReducer from './reducers/user';

const reducers = combineReducers({
    credentials: userReducer,
});

const storeConfig = () => {
    return createStore(reducers);
}

export default storeConfig;