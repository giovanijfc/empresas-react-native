import {
    USER_LOGGED_IN,
    USER_LOGGED_OUT,
    USER_AUTH_IN
} from '../actions/actionsTypes';
import getRealm from '../../services/realm';

const INITIAL_STATE = {
    userLogged: {},
}

const reducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case USER_LOGGED_IN:
            return {
                ...state,
                userLogged: action.user
            };
        case USER_LOGGED_OUT:
            return {
                ...state,
                userLogged: {}
            };
        case USER_AUTH_IN:
            
            return {
                ...state,
                userLogged: action.user,
            };
        default:
            return state;
    }
}

export default reducer;