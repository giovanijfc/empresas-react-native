import {
    USER_LOGGED_IN,
    USER_LOGGED_OUT,
    USER_AUTH_IN
} from './actionsTypes';
import BaseDAO from '../../realm/dao/index';

export const loggedIn = (userLogged) => {
    return {
        type: USER_LOGGED_IN,
        user: userLogged,
    }
}

export const loggedOut = () => {
    BaseDAO.deleteAll();

    return {
        type: USER_LOGGED_OUT,
        user: {}
    }
}

export const authIn = (userLogged) => {
    return {
        type: USER_AUTH_IN,
        user: userLogged,
    }
}