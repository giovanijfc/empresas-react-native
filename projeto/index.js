import React from 'react';
import { AppRegistry } from 'react-native';
import Routes from './src/config/routes';
import { name as appName } from './app.json';
import { Provider } from 'react-redux';
import storeConfig from './src/store/storeConfig';

const store = storeConfig();
const Redux = () => (
    <Provider store={store}>
        <Routes />
    </Provider>
);

AppRegistry.registerComponent(appName, () => Redux);
