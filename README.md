﻿
### Justificativas dependências

* Usados para navegação de telas (React-Navigation):
       * "react-native-gesture-handler": "^1.5.0"
       * "react-navigation": "^4.0.10"
       * "react-navigation-stack": "^1.10.3"

* Usado para salvar dados em um banco local no aplicativo (Realm-DB):
       * "realm": "^3.4.2"

* Usado para estilizar componentes (Styled-Components):
       * "styled-components": "^4.4.1"

* Usado para mensagens rápidas ao usuário (SnackBar):
       * "react-native-snackbar": "^2.0.3"

* Usando para ícones já prontos (React-Native-Vector-Icons):
       * "react-native-vector-icons": "^6.6.0"

* Usando para constrole global de estado (React-Redux):
       * "react-redux": "^7.1.3"
       * "redux": "^4.0.4"

### Executar projeto

* Passos para executar o projeto:
       * git clone https://bitbucket.org/giovanijfc/empresas-react-native.git
       * cd projeto
       * npm install
       * Ativar modo desenvolvedor no celular e permitir depuração via usb       
       * Conectar usb e aceitar permissão de depuração da maquina       
       * Executar no diretório projeto react-native run-android.

* OBS: eu testei somente no android pois estava usando Linux.


		

		





	
